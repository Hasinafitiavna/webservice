package com.example.demo5.avion;

public class UsefulEntity {
    Boolean state;
    int id;
    public UsefulEntity(Boolean state, int id) {
        this.state = state;
        this.id = id;
    }

    public UsefulEntity(){}

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
