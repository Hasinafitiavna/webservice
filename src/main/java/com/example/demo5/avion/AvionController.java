package com.example.demo5.avion;

import com.example.demo5.avion.Avion;
import com.example.demo5.avion.AvionService;
import com.example.demo5.avion.UsefulEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/avions")
@CrossOrigin("*")
public class AvionController {

    private AvionService avionService;

    public AvionController(AvionService avionService){
        super();
        this.avionService=avionService;
    }

    @PostMapping("save")
    public ResponseEntity<Avion> saveAvion(@RequestBody Avion avion){
        return new ResponseEntity<Avion>(avionService.saveAvion(avion), HttpStatus.CREATED);
    }

    @GetMapping
    public List<Avion> getAllAvion(){
        return avionService.getAllAvion();
    }

    @GetMapping("{id}")
    public ResponseEntity<Avion> getAvionById(@PathVariable("id") int id){
        return new ResponseEntity<Avion>(avionService.getAvionById(id), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<Avion> updateAvion(@PathVariable("id") int id, @RequestBody Avion avion){
        return new ResponseEntity<Avion>(avionService.updateAvion(avion, id), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteAvion(@PathVariable("id") int id){
        avionService.deleteAvion(id);
        return new ResponseEntity<String>("Avion number "+ id+"" +" has been deleted successfully", HttpStatus.OK);
    }

    @GetMapping("{nom}/{pass}")
    public ResponseEntity<UsefulEntity> loginAvion(@PathVariable("nom") String nom,@PathVariable("pass") String pass) {
        UsefulEntity res = avionService.loginAvion(nom,pass);
        return new ResponseEntity<UsefulEntity>(res,HttpStatus.OK);
    }
}
