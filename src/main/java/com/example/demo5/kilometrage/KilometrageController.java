package com.example.demo5.kilometrage;


import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/kilometrage")
@CrossOrigin("*")
public class KilometrageController {
    private KilometrageService kilometrageService;

    public KilometrageController(KilometrageService kilometrageService){
        super();
        this.kilometrageService=kilometrageService;
    }

    @GetMapping("{id}")
    public ArrayList<Kilometrage> getkm(@PathVariable("id") int id){
        return kilometrageService.getkm(id);
    }
}
