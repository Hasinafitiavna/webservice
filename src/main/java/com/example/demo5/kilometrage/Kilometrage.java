package com.example.demo5.kilometrage;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="kilometrage")

public class Kilometrage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //@OneToOne (targetEntity = Avion.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "idavion", referencedColumnName = "id")
    private Integer idavion;

    @Column(nullable = false, length = 45, name = "km")
    private Integer km;


    public Kilometrage(Integer idavion, Integer km) {
        this.idavion = idavion;
        this.km = km;
    }

    public Kilometrage(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdavion() {
        return idavion;
    }

    public void setIdavion(Integer idavion) {
        this.idavion = idavion;
    }

    public Integer getKm() {
        return km;
    }

    public void setKm(Integer km) {
        this.km = km;
    }
}
