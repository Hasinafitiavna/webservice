package com.example.demo5.kilometrage;

import java.util.ArrayList;

public interface KilometrageService {
    ArrayList<Kilometrage> getkm(int id);
}
