package com.example.demo5.kilometrage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.ArrayList;

@Service
@CrossOrigin("*")
public class KilometrageServiceImpl implements KilometrageService{
    private KilometrageRepository kilometrageRepository;
    @Autowired
    public KilometrageServiceImpl(KilometrageRepository kilometrageRepository){
        super();
        this.kilometrageRepository = kilometrageRepository;
    }

    @Override
    public ArrayList<Kilometrage> getkm(int id) {
        return kilometrageRepository.gethis(id);
    }
}
