package com.example.demo5.assurance;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;
@RestController
@RequestMapping("/api/assurances")
@CrossOrigin("*")
public class AssuranceController {

    private AssuranceService assuranceService;

    private AssuranceRep assuranceRep;
    public AssuranceController(AssuranceService assuranceService,AssuranceRep assuranceRep) {
        this.assuranceService = assuranceService;
        this.assuranceRep=assuranceRep;
    }


    @PostMapping("save")
    public ResponseEntity<Assurance> saveAssurance(@RequestBody Assurance assurance){
        return new ResponseEntity<Assurance>(assuranceService.saveAssurance(assurance), HttpStatus.CREATED);
    }
    @GetMapping("{mois}")
    public List<Assurance> getAssuranceByMonth(@PathVariable("mois") int mois){
        return assuranceRep.getAssuranceExpires(mois);
    }
    @GetMapping
    public List<Assurance> getAllAssurances(){
        return assuranceService.getAllAssurances();
    }

//    @GetMapping("{id}")
//    public ResponseEntity<Assurance> getAssuranceById(@PathVariable("id") int id){
//        return new ResponseEntity<Assurance>(assuranceService.getAssuranceById(id), HttpStatus.OK);
//    }

    @PutMapping("{id}")
    public ResponseEntity<Assurance> updateAssurance(@PathVariable("id") int id,@RequestBody Assurance vehicule){
        return new ResponseEntity<Assurance>(assuranceService.updateAssurance(vehicule, id), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteVehicule(@PathVariable("id") int id){
        assuranceService.deleteAssurance(id);
        return new ResponseEntity<String>("Assurance number "+ id+"" +" has been deleted successfully", HttpStatus.OK);
    }
}
