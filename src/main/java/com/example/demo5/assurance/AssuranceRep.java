package com.example.demo5.assurance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.util.List;

public interface AssuranceRep extends JpaRepository<Assurance,Integer> {
    @Query(value = "SELECT * from assurance join avion on assurance.idavion=avion.id where (SELECT date_part ('year', f) * 12+ date_part ('month', f) FROM age (dateexpiration, current_date) f)=?1",nativeQuery = true)
    public List<Assurance> getAssuranceExpires(int valeurMois);
}
