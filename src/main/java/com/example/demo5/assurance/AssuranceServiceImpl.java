package com.example.demo5.assurance;

import exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssuranceServiceImpl implements AssuranceService{

    private AssuranceRepository assuranceRepository;
    @Autowired
    public AssuranceServiceImpl(AssuranceRepository assuranceRepository) {
        super();
        this.assuranceRepository = assuranceRepository;
    }

    @Override
    public Assurance saveAssurance(Assurance assurance) {
        return assuranceRepository.save(assurance);
    }

    @Override
    public List<Assurance> getAllAssurances(){
        return (List<Assurance>) assuranceRepository.findAll();
    }

    @Override
    public Assurance getAssuranceById(int id){
        return assuranceRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Vehicule", "Id", id));
    }

    @Override
    public Assurance updateAssurance(Assurance assurance, int id) {
        Assurance existingAssurance = assuranceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Assurance", "Id", id));
        existingAssurance.setDateexpiration(assurance.getDateexpiration());
        existingAssurance.setIdavion(assurance.getIdavion());

        assuranceRepository.save(existingAssurance);
        return existingAssurance;
    }


    @Override
    public void deleteAssurance(int id) {
        assuranceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Assurance", "Id", id));
        assuranceRepository.deleteById(id);
    }
//    @Override
//    public void selectParMois(int id){
//        return (List<Assurance>) assuranceRepository.find
//    }

}
