package com.example.demo5.assurance;


import com.example.demo5.avion.Avion;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name="assurance")
public class Assurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, name = "dateexpiration")
    private Date dateexpiration;

//    @OneToOne (targetEntity = Avion.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "idavion", referencedColumnName = "id",table = "avion")
    private Integer idavion;
//
    public Assurance(Date dateexpiration, Integer idavion) {
        this.dateexpiration = dateexpiration;
        this.idavion = idavion;
    }

    public Assurance() {

    }

    public Assurance(Integer id, Date dateexpiration) {
        this.id = id;
        this.dateexpiration = dateexpiration;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateexpiration() {
        return dateexpiration;
    }

    public void setDateexpiration(Date dateexpiration) {
        this.dateexpiration = dateexpiration;
    }

    public Integer getIdavion() {
        return idavion;
    }

    public void setIdavion(Integer idavion) {
        this.idavion = idavion;
    }

    public static void main(String[] args) {
//        AssuranceRep rep=ne

    }
}
