package com.example.demo5.assurance;

import java.util.List;

public interface AssuranceService {
    Assurance saveAssurance(Assurance assurance);
    List<Assurance> getAllAssurances();


    Assurance getAssuranceById(int id);
    Assurance updateAssurance(Assurance assurance, int id);
    void deleteAssurance(int id);
}
