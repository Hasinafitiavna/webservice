package com.example.demo5.entities;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="maintenance")

public class Maintenance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 45, name = "nom")
    private String nom;

    public Maintenance(String nom) {
        this.nom = nom;
    }

    public Maintenance() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
