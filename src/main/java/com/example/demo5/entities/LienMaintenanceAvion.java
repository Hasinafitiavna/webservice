package com.example.demo5.entities;


import com.example.demo5.avion.Avion;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name="lienmaintenanceavion")

public class LienMaintenanceAvion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne (targetEntity = Avion.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "idavion", referencedColumnName = "id")
    private Integer idavion;

    @OneToOne (targetEntity = Maintenance.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "idmaintenance", referencedColumnName = "id")
    private Integer idmaintenace;

    @Column(nullable = false, name = "datemaintenance")
    private Date datemaintenance;

    public LienMaintenanceAvion(Integer idavion, Integer idmaintenace, Date datemaintenance) {
        this.idavion = idavion;
        this.idmaintenace = idmaintenace;
        this.datemaintenance = datemaintenance;
    }

    public LienMaintenanceAvion() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdavion() {
        return idavion;
    }

    public void setIdavion(Integer idavion) {
        this.idavion = idavion;
    }

    public Integer getIdmaintenace() {
        return idmaintenace;
    }

    public void setIdmaintenace(Integer idmaintenace) {
        this.idmaintenace = idmaintenace;
    }

    public Date getDatemaintenance() {
        return datemaintenance;
    }

    public void setDatemaintenance(Date datemaintenance) {
        this.datemaintenance = datemaintenance;
    }
}
