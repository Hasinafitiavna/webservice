package com.example.demo5.entities.pers;

import java.util.List;

public interface PersonnelService {
    UsefulEntity login(String nom, String pass);
    List<Personnel> getAllPersonnel();
}
