package com.example.demo5.entities.pers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Service
@CrossOrigin("*")
public class PersonnelServiceImpl implements PersonnelService{
    private PersonnelRepository personnelRepository;

    @Autowired
    public PersonnelServiceImpl(PersonnelRepository personnelRepository){
        super();
        this.personnelRepository = personnelRepository;
    }

    @Override
    public List<Personnel> getAllPersonnel(){
        return (List<Personnel>) personnelRepository.findAll();
    }

    @Override
    public com.example.demo5.entities.pers.UsefulEntity login(String nom, String pass) {
        List<Personnel> liste = getAllPersonnel();
        Boolean res = false;
        int id=0;
        for(int i=0; i<liste.size(); i++){
            if((liste.get(i).getNom().equals(nom))&&(liste.get(i).getPass().equals(pass))){
                res = true;
                id = liste.get(i).getId();
                System.out.println("Successful login, personnel name: "+liste.get(i).getNom() + ", id= " + liste.get(i).getId());
                break;
            }
        }
        if(!res){
            System.out.println("you Personnel name: " +nom+ " do not exist in our database");
        }
        return new com.example.demo5.entities.pers.UsefulEntity(res,id);
    }
}
