package com.example.demo5.entities.pers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/personnels")
@CrossOrigin("*")
public class PersonnelController {

    private PersonnelService personnelService;

    public PersonnelController(PersonnelService personnelService){
        super();
        this.personnelService=personnelService;
    }

    @GetMapping
    public List<Personnel> getAllPersonnel(){
        return personnelService.getAllPersonnel();
    }

    @GetMapping("{nom}/{pass}")
    public ResponseEntity<com.example.demo5.entities.pers.UsefulEntity> login(@PathVariable("nom") String nom, @PathVariable("pass") String pass) {
        com.example.demo5.entities.pers.UsefulEntity res = personnelService.login(nom,pass);
        return new ResponseEntity<com.example.demo5.entities.pers.UsefulEntity>(res, HttpStatus.OK);
    }
}
