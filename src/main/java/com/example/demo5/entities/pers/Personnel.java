package com.example.demo5.entities.pers;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="personnel")

public class Personnel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 45, name = "nom")
    private String nom;

    @Column(nullable = false, length = 45, name = "pass")
    private String pass;

    public Personnel(){}

    public Personnel(String nom) {
        this.nom = nom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
