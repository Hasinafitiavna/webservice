DROP TABLE avion CASCADE;
DROP TABLE personnel CASCADE;
DROP TABLE kilometrage CASCADE;
DROP TABLE maintenance CASCADE;
DROP TABLE lienmaintenanceavion CASCADE;
DROP TABLE assurance CASCADE;

create table avion(
    id SERIAL,
    nom VARCHAR(45),
    PRIMARY KEY (id)
);

CREATE TABLE personnel(
    id SERIAL,
    nom VARCHAR(45),
    pass VARCHAR(45),
    PRIMARY KEY(id)
);

CREATE TABLE kilometrage(
    id SERIAL,
    idavion INT,
    km INT,
    PRIMARY KEY(id),
    FOREIGN KEY(idavion) REFERENCES avion(id)
);

CREATE TABLE maintenance(
    id SERIAL,
    nom VARCHAR(45),
    PRIMARY KEY(id)
);

CREATE TABLE lienmaintenanceavion(
    id INT,
    idavion INT,
    idmaintenance INT,
    datemaintenance DATE,
    PRIMARY KEY(id),
    FOREIGN KEY(idavion) REFERENCES avion(id),
    FOREIGN KEY(idmaintenance) REFERENCES maintenance(id)
);

CREATE TABLE assurance(
    id SERIAL,
    dateexpiration DATE,
    idavion INT,
    FOREIGN KEY(idavion) REFERENCES avion(id)
);

insert into personnel(nom,pass) values ('james','j');

insert into avion(nom) values ('airbus01');
insert into avion(nom) values ('airbus02');
insert into avion(nom) values ('airbus03');

insert into kilometrage(idavion,km) values (1,10);
insert into kilometrage(idavion,km) values (2,20);
insert into kilometrage(idavion,km) values (3,30);
select * from avion join kilometrage k on avion.id = k.idavion;
insert into assurance( dateexpiration, idavion) values ('2023-02-01',1);
insert into assurance( dateexpiration, idavion) values ('2023-04-01',3);
